#include <cstdlib>
#include "ap_int.h"
#include "algo_top.h"

//    eta:        0  1  2  3  4   5  6  7  8   9 10 11 12  13 14 15 16  17 18 19 20  21 22 23 24  25 26 27 28  29 30 31 32 33
// phi           ____________________________________________________________________________________________________________
// 0             |                                                    |                                                     |
// 1             |                                                    |                                                     |
// 2             |                         0                          |                           5                         |
// 3             |                                                    |                                                     |
//               -----------------------------------------------------|-----------------------------------------------------|
// 4             |                                                    |                                                     |
// 5             |                         1                          |                           6                         |
// 6             |                                                    |                                                     |
// 7             |                                                    |                                                     |
//               ------------------------------------------------------------------------------------------------------------
// 8             |                                                    |                                                     |
// 9             |                         2                          |                           7                         |
// 10            |                                                    |                                                     |
// 11            |                                                    |                                                     |
//               -----------------------------------------------------|-----------------------------------------------------|
// 12            |                                                    |                                                     |
// 13            |                         3                          |                           8                         |
// 14            |                                                    |                                                     |
// 15            |                                                    |                                                     |
//               _____________________________________________________|_____________________________________________________|
// 16            |                                                    |                                                     |
// 17            |                                                    |                                                     |
// 18            |                         4                          |                           9                         |
// 19            |                                                    |                                                     |
//               |____________________________________________________|_____________________________________________________|
//
// 8 PFclusters are created in one 21x8 (2+17+2 x 2+4+2) --> Total 6x8 = 48 pfclusters

GCTPFPrimitives bestOf2(GCTPFPrimitives &t0, GCTPFPrimitives &t1) {
#pragma HLS INLINE

GCTPFPrimitives x;
ap_uint<12> t0_energy = t0.et();
ap_uint<12> t1_energy = t1.et();

x.data = (t0_energy > t1_energy) ? t0.data : t1.data;
return x;

}

void getPeakOfStrip(GCTPFPrimitives etaStrip[8], GCTPFPrimitives &bestAll){
#pragma HLS ARRAY_PARTITION variable=etaStrip complete dim=0
#pragma HLS INLINE

GCTPFPrimitives best12   = bestOf2(etaStrip[1], etaStrip[2]);
GCTPFPrimitives best34   = bestOf2(etaStrip[3], etaStrip[4]);
GCTPFPrimitives best56   = bestOf2(etaStrip[5], etaStrip[6]);
GCTPFPrimitives best1234 = bestOf2(best12, best34);

bestAll  = bestOf2(best1234, best56);

}

GCTPFPrimitives getPeakBin(GCTPFPrimitives etaStripPeak[19]){
#pragma HLS ARRAY_PARTITION variable=etaStripPeak complete dim=0
#pragma HLS INLINE

GCTPFPrimitives best01             = bestOf2(etaStripPeak[0], etaStripPeak[1]);
GCTPFPrimitives best23             = bestOf2(etaStripPeak[2], etaStripPeak[3]);
GCTPFPrimitives best45             = bestOf2(etaStripPeak[4], etaStripPeak[5]);
GCTPFPrimitives best67             = bestOf2(etaStripPeak[6], etaStripPeak[7]);
GCTPFPrimitives best89             = bestOf2(etaStripPeak[8], etaStripPeak[9]);
GCTPFPrimitives best1011           = bestOf2(etaStripPeak[10], etaStripPeak[11]);
GCTPFPrimitives best1213           = bestOf2(etaStripPeak[12], etaStripPeak[13]);
GCTPFPrimitives best1415           = bestOf2(etaStripPeak[14], etaStripPeak[15]);
GCTPFPrimitives best1617           = bestOf2(etaStripPeak[16], etaStripPeak[17]);
GCTPFPrimitives best0123           = bestOf2(best01, best23);
GCTPFPrimitives best4567           = bestOf2(best45, best67);
GCTPFPrimitives best891011         = bestOf2(best89, best1011);
GCTPFPrimitives best12131415       = bestOf2(best1213, best1415);
GCTPFPrimitives best01234567       = bestOf2(best0123, best4567);
GCTPFPrimitives best01234567891011 = bestOf2(best01234567, best891011);
GCTPFPrimitives best121314151617   = bestOf2(best12131415, best1617);
GCTPFPrimitives best12131415161718 = bestOf2(best121314151617, etaStripPeak[18]);

GCTPFPrimitives bestAll = bestOf2(best01234567891011, best12131415161718);

return bestAll;

}

void getPeakPosition(GCTPFPrimitives region[21][8], GCTPFPrimitives &max){
#pragma HLS ARRAY_PARTITION variable=region complete dim=0
#pragma HLS INLINE

GCTPFPrimitives etaPeak[19];
#pragma HLS ARRAY_PARTITION variable=etaPeak complete dim=0

for(idx i=0; i<19; i++){
    #pragma HLS unroll
	getPeakOfStrip(&region[i+1][0], etaPeak[i]);
}

max = getPeakBin(etaPeak);

}

void initStructure(TowerPrimitives temp[21][8], GCTPFPrimitives region[21][8]){
#pragma HLS ARRAY_PARTITION variable=temp complete dim=0
#pragma HLS ARRAY_PARTITION variable=region complete dim=0
#pragma HLS INLINE

for(idx i=0; i<8; i++){
	#pragma HLS unroll
	for(idx j=0; j<21; j++){
   	   #pragma HLS unroll
       region[j][i].data.range(11, 0)  = temp[j][i].et();
       region[j][i].data.range(18, 12) = (ap_uint<7>) j;
       region[j][i].data.range(23, 19) = (ap_uint<5>) i;
	}
}

}

void getEt(TowerPrimitives temp[21][8], ap_uint<7> eta, ap_uint<5> phi, ap_uint<12> &energy){
#pragma HLS ARRAY_PARTITION variable=temp complete dim=0
#pragma HLS INLINE

ap_uint<12> cube[3][3];
#pragma HLS ARRAY_PARTITION variable=cube complete dim=0

for(idx i=0; i<19; i++){
  for(idx j=0; j<6; j++){
    if (i+1 == eta && j+1 == phi){
      for(idx row=0; row<3; row++){
        #pragma HLS unroll
          for(idx col=0; col<3; col++){
          	  #pragma HLS unroll
        	  cube[row][col] = temp[i+row][j+col].et();
          }
      	}
    }
  }
}

energy = cube[0][0] + cube[0][1] + cube[0][2] +
         cube[1][0] + cube[1][1] + cube[1][2] +
         cube[2][0] + cube[2][1] + cube[2][2];

}

void RemoveTmp(TowerPrimitives temp[21][8], ap_uint<7> eta, ap_uint<5> phi){
#pragma HLS ARRAY_PARTITION variable=temp complete dim=0
#pragma HLS INLINE

for(idx i=0; i<21; i++){
  if(i+1 >= eta && i <= eta+1){
    for(idx j=0; j<8; j++){
      if(j+1 >= phi && j <= phi+1) {
        temp[i][j].data = 0;
      }
    }
  }
}

}

void recoPfcluster(TowerPrimitives temporary[21][8], GCTPFPrimitives &pfclusterReturn){
#pragma HLS ARRAY_PARTITION variable=temporary complete dim=0
#pragma HLS INLINE

GCTPFPrimitives region[21][8];
#pragma HLS ARRAY_PARTITION variable=region complete dim=0

initStructure(temporary, region);

GCTPFPrimitives regionMax;
getPeakPosition(region, regionMax);

ap_uint<12> pfcluster_et = 0;
getEt(temporary, regionMax.eta(), regionMax.phi(), pfcluster_et);

RemoveTmp(temporary, regionMax.eta(), regionMax.phi());

if(!(regionMax.eta() >= 2 && regionMax.eta() <= 18 && regionMax.phi() >= 2 && regionMax.phi() <= 5)) pfcluster_et = 0;

pfclusterReturn.data.range(11, 0)  = pfcluster_et;
pfclusterReturn.data.range(18, 12) = regionMax.eta();
pfclusterReturn.data.range(23, 19) = regionMax.phi();

}

void pfcluster(TowerPrimitives RCTTowers[21][8], ap_uint<7> etaoffset, ap_uint<5> phioffset, GCTPFPrimitives Pfclusters[8]) {
#pragma HLS ARRAY_PARTITION variable=RCTTowers complete dim=0
#pragma HLS ARRAY_PARTITION variable=Pfclusters complete dim=0

GCTPFPrimitives Internalcluster[8];
#pragma HLS ARRAY_PARTITION variable=Internalcluster complete dim=0

recoPfcluster(RCTTowers, Internalcluster[0]);
Pfclusters[0].data.range(11, 0)  = Internalcluster[0].et();
Pfclusters[0].data.range(18, 12) = Internalcluster[0].eta() - 2 + etaoffset;
Pfclusters[0].data.range(23, 19) = Internalcluster[0].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[1]);
Pfclusters[1].data.range(11, 0)  =  Internalcluster[1].et();
Pfclusters[1].data.range(18, 12) =  Internalcluster[1].eta() - 2 + etaoffset;
Pfclusters[1].data.range(23, 19) =  Internalcluster[1].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[2]);
Pfclusters[2].data.range(11, 0)  =  Internalcluster[2].et();
Pfclusters[2].data.range(18, 12) =  Internalcluster[2].eta() - 2 + etaoffset;
Pfclusters[2].data.range(23, 19) =  Internalcluster[2].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[3]);
Pfclusters[3].data.range(11, 0)  =  Internalcluster[3].et();
Pfclusters[3].data.range(18, 12) =  Internalcluster[3].eta() - 2 + etaoffset;
Pfclusters[3].data.range(23, 19) =  Internalcluster[3].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[4]);
Pfclusters[4].data.range(11, 0)  =  Internalcluster[4].et();
Pfclusters[4].data.range(18, 12) =  Internalcluster[4].eta() - 2 + etaoffset;
Pfclusters[4].data.range(23, 19) =  Internalcluster[4].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[5]);
Pfclusters[5].data.range(11, 0)  =  Internalcluster[5].et();
Pfclusters[5].data.range(18, 12) =  Internalcluster[5].eta() - 2 + etaoffset;
Pfclusters[5].data.range(23, 19) =  Internalcluster[5].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[6]);
Pfclusters[6].data.range(11, 0)  =  Internalcluster[6].et();
Pfclusters[6].data.range(18, 12) =  Internalcluster[6].eta() - 2 + etaoffset;
Pfclusters[6].data.range(23, 19) =  Internalcluster[6].phi() - 2 + phioffset;

recoPfcluster(RCTTowers, Internalcluster[7]);
Pfclusters[7].data.range(11, 0)  =  Internalcluster[7].et();
Pfclusters[7].data.range(18, 12) =  Internalcluster[7].eta() - 2 + etaoffset;
Pfclusters[7].data.range(23, 19) =  Internalcluster[7].phi() - 2 + phioffset;

}
