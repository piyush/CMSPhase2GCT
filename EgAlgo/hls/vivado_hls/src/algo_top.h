#ifndef _ALGO_TOP_H_
#define _ALGO_TOP_H_

#include <iostream>
#include "ap_int.h"
#include "ap_utils.h"
#include "algo_top_parameters.h"

#define N_RCTCARDS_PHI 5 
#define N_FIBERS_RCT 4 
#define N_RCTTOWERS_FIBER 17 
#define N_RCTCLUSTERS_FIBER 2 
#define N_PFCLUSTERS_FIBER 2

#define N_FIBERS_GCTCORR 24

#define N_FIBERS_GCTTOTAL 40
#define N_FIBERS_GCT_POSITIVE 20
#define N_FIBERS_GCT_NEGATIVE 20
#define N_GCT_ETA 34
#define N_GCT_PHI 20

typedef ap_uint<10> idx;

using namespace std;

class RCTClusterPrimitives{
   public:
    ap_uint<60> data;

    RCTClusterPrimitives(){
        data = 0;
    }

    RCTClusterPrimitives& operator=(const RCTClusterPrimitives& rhs){
        data = rhs.data;
        return *this;
    }

    RCTClusterPrimitives(ap_uint<12> clusterEnergy, ap_uint<5> towerEta, ap_uint<2> towerPhi, ap_uint<3> clusterEta, ap_uint<3> clusterPhi, ap_uint<3> satur, ap_uint<15> et5x5, ap_uint<15> et2x5, ap_uint<2> brems){
        data = (clusterEnergy) | 
      (((ap_uint<60>) towerEta)    << 12) |
      (((ap_uint<60>) towerPhi)    << 17) |
      (((ap_uint<60>) clusterEta)  << 19) |
      (((ap_uint<60>) clusterPhi)  << 22) |
      (((ap_uint<60>) satur)       << 25) |
      (((ap_uint<60>) et5x5)       << 28) |
      (((ap_uint<60>) et2x5)       << 43) |
      (((ap_uint<60>) brems)       << 58);
    }

    ap_uint<12> clusterEnergy() {return (data & 0xFFF);}
    ap_uint<5>  towerEta() {return ((data >> 12) & 0x1F);}
    ap_uint<2>  towerPhi() {return ((data >> 17) & 0x3);}
    ap_uint<3>  clusterEta() {return ((data >> 19) & 0x7);}
    ap_uint<3>  clusterPhi() {return ((data >> 22) & 0x7);}
    ap_uint<3>  satur() {return ((data >> 25) & 0x7);}
    ap_uint<15> et5x5() {return ((data >> 28) & 0x7FFF);}
    ap_uint<15> et2x5() {return ((data >> 43) & 0x7FFF);}
    ap_uint<2>  brems() {return ((data >> 58) & 0x3);}
	
};

class TowerPrimitives{
   public:
      ap_uint<16> data;

      TowerPrimitives(){
      data = 0;
   }

      TowerPrimitives& operator=(const TowerPrimitives& rhs){
      data = rhs.data;
      return *this;
    }

      TowerPrimitives(ap_uint<4> hoe, ap_uint<12> et){
        data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
    }

    ap_uint<12> et()  {return (data & 0xFFF);}
    ap_uint<4>  hoe() {return (data >> 12) & 0xF;}

};

typedef struct{
	TowerPrimitives towers[N_RCTTOWERS_FIBER];
} TowersInPhi;

typedef struct{
    RCTClusterPrimitives RCTclusters[N_RCTCLUSTERS_FIBER];
} ClustersInPhi;

class GCTclusterPrimitives{

   public:
    ap_uint<44> data;

    GCTclusterPrimitives(){
        data = 0;
    }

    GCTclusterPrimitives& operator=(const GCTclusterPrimitives& rhs){
        data = rhs.data;
        return *this;
    }

    GCTclusterPrimitives(ap_uint<12> et, ap_uint<1> towEtaNeg, ap_uint<6> towerEta, ap_uint<5> towerPhi, ap_uint<3> crEta, ap_uint<3> crPhi, ap_uint<3> satur, ap_uint<3> hoe, ap_uint<3> iso, ap_uint<3> shape, ap_uint<2> brems){
      data = (et) |
      (((ap_uint<64>) towEtaNeg)  << 12) | 
      (((ap_uint<64>) towerEta)   << 13) | 
      (((ap_uint<64>) towerPhi)   << 19) | 
      (((ap_uint<64>) crEta)      << 24) | 
      (((ap_uint<64>) crPhi)      << 27) |
      (((ap_uint<64>) satur)      << 30) |
      (((ap_uint<64>) hoe)        << 33) |
      (((ap_uint<64>) iso)        << 36) |
      (((ap_uint<64>) shape)      << 39) |
      (((ap_uint<64>) brems)      << 42);
    }

    ap_uint<12> et()       {return (data & 0xFFF);}
    ap_uint<1> towEtaNeg() {return ((data >> 12) & 0x1);}
    ap_uint<6> towerEta()  {return ((data >> 13) & 0x3F);}
    ap_uint<5> towerPhi()  {return ((data >> 19) & 0x1F);}
    ap_uint<3> crEta()     {return ((data >> 24) & 0x7);}
    ap_uint<3> crPhi()     {return ((data >> 27) & 0x7);}
    ap_uint<3> satur()     {return ((data >> 30) & 0x7);}
    ap_uint<3> hoe()       {return ((data >> 33) & 0x7);}
    ap_uint<3> iso()       {return ((data >> 36) & 0x7);}
    ap_uint<3> shape()     {return ((data >> 39) & 0x7);}
    ap_uint<2> brems()     {return ((data >> 42) & 0x3);}
	
};

typedef struct{
    GCTclusterPrimitives GCTclusters[N_RCTCLUSTERS_FIBER];
} GCTClustersInPhi;

class GCTPFPrimitives{
   public:
      ap_uint<24> data; 
   
   GCTPFPrimitives(){
       data = 0;
   }

   GCTPFPrimitives& operator=(const GCTPFPrimitives& rhs){
      data = rhs.data;
      return *this;
   }

   GCTPFPrimitives(ap_uint<12> et, ap_uint<7> eta, ap_uint<5> phi){
      data = ((ap_uint<24>) phi << 19) | ((ap_uint<24>) eta << 12) | ((ap_uint<24>) et);
   }

   ap_uint<12> et() {return (data & 0xFFF);}
   ap_uint<7> eta() {return ((data >> 12) & 0x7F);}
   ap_uint<5> phi() {return ((data >> 19) & 0x1F);}

};

void algo_top(ap_uint<576> link_in[N_INPUT_LINKS], ap_uint<576> link_out_CORR[N_OUTPUT_LINKS], ap_uint<576> link_out_PF[N_OUTPUT_LINKS], ap_uint<576> link_out_JETS[N_OUTPUT_LINKS_JETS]);

void pfcluster(TowerPrimitives RCTtowers[21][8], ap_uint<7> etaoffset, ap_uint<5> phioffset, GCTPFPrimitives Pfclusters[8]);

void makePfclusters(TowerPrimitives GCTTowersRegion[N_GCT_ETA][N_GCT_PHI], GCTPFPrimitives TotalPfclusterPOS[24], GCTPFPrimitives TotalPfclusterNEG[24]);

#endif
