#ifndef __ALGO_TOP_PARAMETERS_H__
#define __ALGO_TOP_PARAMETERS_H__

/*
 * algo_top_parameters.h
 *
 * Defines the algorithm related configuration parameters.
 */

/** Common algorithm definitions, do not remove **/
// Un-comment to have algo as a passthrough for testing purposes
//#define ALGO_PASSTHROUGH

// Number of data words per processing cycle/frame
const int N_WORDS_PER_FRAME	= 9;

/** Algorithm specific parameters **/
#define TOWERS_IN_ETA 3
#define TOWERS_IN_PHI 4

/** More common algorithm definitions, do not remove **/
// 5x2 RCT x4 = 40 inputs
#define N_INPUT_LINKS	    40
#define N_OUTPUT_LINKS_CORR 24
#define N_OUTPUT_LINKS_PF   24
#define N_OUTPUT_LINKS      24

#define N_OUTPUT_LINKS_JETS 16



// 25G inputs
#define N_INPUT_WORDS_PER_FRAME 9
// 25G outputs
#define N_OUTPUT_WORDS_PER_FRAME 9

#endif

