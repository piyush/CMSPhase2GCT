#include "algo_top.h"

void makePfclusters(TowerPrimitives GCTTowersRegion[N_GCT_ETA][N_GCT_PHI], GCTPFPrimitives TotalPfclusterPOS[24], GCTPFPrimitives TotalPfclusterNEG[24]){
#pragma HLS ARRAY_PARTITION variable=GCTTowersRegion complete dim=0
#pragma HLS ARRAY_PARTITION variable=TotalPfclusterPOS complete dim=0
#pragma HLS ARRAY_PARTITION variable=TotalPfclusterNEG complete dim=0

TowerPrimitives RCTtowers1[21][8];
TowerPrimitives RCTtowers2[21][8];
TowerPrimitives RCTtowers3[21][8];
TowerPrimitives RCTtowers6[21][8];
TowerPrimitives RCTtowers7[21][8];
TowerPrimitives RCTtowers8[21][8];

#pragma HLS ARRAY_PARTITION variable=RCTtowers1  complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTtowers2  complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTtowers3  complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTtowers6  complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTtowers7  complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTtowers8  complete dim=0

for(idx i=0; i<19; i++){
  for(idx j=0; j<8; j++){
    #pragma HLS unroll
    RCTtowers1[i+2][j] = GCTTowersRegion[i][j+2];
    RCTtowers2[i+2][j] = GCTTowersRegion[i][j+6];
    RCTtowers3[i+2][j] = GCTTowersRegion[i][j+10];
  }
}

for(idx i=0; i<19; i++){
  for(idx j=0; j<8; j++){
    #pragma HLS unroll
    RCTtowers6[i][j] = GCTTowersRegion[i+15][j+2];
    RCTtowers7[i][j] = GCTTowersRegion[i+15][j+6];
    RCTtowers8[i][j] = GCTTowersRegion[i+15][j+10];
  }
}

GCTPFPrimitives Pfclusters1[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters1  complete dim=0
pfcluster(RCTtowers1, 0, 4, Pfclusters1);

GCTPFPrimitives Pfclusters2[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters2  complete dim=0
pfcluster(RCTtowers2, 0, 8, Pfclusters2);

GCTPFPrimitives Pfclusters3[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters3  complete dim=0
pfcluster(RCTtowers3, 0, 12, Pfclusters3);

GCTPFPrimitives Pfclusters6[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters6  complete dim=0
pfcluster(RCTtowers6, 17, 4, Pfclusters6);

GCTPFPrimitives Pfclusters7[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters7  complete dim=0
pfcluster(RCTtowers7, 17, 8, Pfclusters7);

GCTPFPrimitives Pfclusters8[8];
#pragma HLS ARRAY_PARTITION variable=Pfclusters8  complete dim=0
pfcluster(RCTtowers8, 17, 12, Pfclusters8);

// OUTPUT

for(idx i=0; i<8; i++){
  #pragma HLS UNROLL
  TotalPfclusterPOS[i]    = Pfclusters1[i];
  TotalPfclusterPOS[8+i]  = Pfclusters2[i];
  TotalPfclusterPOS[16+i] = Pfclusters3[i];
}

for(idx i=0; i<8; i++){
  #pragma HLS UNROLL
  TotalPfclusterNEG[i]    = Pfclusters6[i];
  TotalPfclusterNEG[8+i]  = Pfclusters7[i];
  TotalPfclusterNEG[16+i] = Pfclusters8[i];
}

}
