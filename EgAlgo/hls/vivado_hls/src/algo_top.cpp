#include "algo_top_parameters.h"
#include "algo_top.h"

void prsOutLinksCORR(ap_uint<576> link_in[N_OUTPUT_LINKS], ap_uint<576> link_out[N_OUTPUT_LINKS]){
#pragma HLS ARRAY_PARTITION variable=link_in complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_out complete dim=0
#pragma HLS PIPELINE II=9
#pragma HLS LATENCY min=9

for(idx i=0; i<N_OUTPUT_LINKS; i++){
    #pragma HLS unroll
    link_out[i] = link_in[i];
}
}

void TowerBuffer(TowerPrimitives &TowerIn, TowerPrimitives &TowerOut){
#pragma HLS PIPELINE
#pragma HLS LATENCY min=1

        TowerOut.data = TowerIn.data;

}

void makePFTowerRegion(TowersInPhi PosRCTTwr0PF[N_FIBERS_RCT], TowersInPhi PosRCTTwr1PF[N_FIBERS_RCT],
                       TowersInPhi PosRCTTwr2PF[N_FIBERS_RCT], TowersInPhi PosRCTTwr3PF[N_FIBERS_RCT],
                       TowersInPhi PosRCTTwr4PF[N_FIBERS_RCT], TowersInPhi NegRCTTwr0PF[N_FIBERS_RCT],
                       TowersInPhi NegRCTTwr1PF[N_FIBERS_RCT], TowersInPhi NegRCTTwr2PF[N_FIBERS_RCT],
                       TowersInPhi NegRCTTwr3PF[N_FIBERS_RCT], TowersInPhi NegRCTTwr4PF[N_FIBERS_RCT],
                       TowerPrimitives GCTTowersRegion[N_GCT_ETA][N_GCT_PHI]){

#pragma HLS ARRAY_PARTITION variable=GCTTowersRegion complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr0PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr1PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr2PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr3PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr4PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr0PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr1PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr2PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr3PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr4PF complete dim=0

for(idx i=0; i<N_FIBERS_RCT; i++){
    for(idx j=0; j<(N_GCT_ETA/2); j++){
                #pragma HLS unroll
        GCTTowersRegion[j][i].data       = NegRCTTwr0PF[i].towers[j].data;
        GCTTowersRegion[j][i+4].data     = NegRCTTwr1PF[i].towers[j].data;
        GCTTowersRegion[j][i+8].data     = NegRCTTwr2PF[i].towers[j].data;
        GCTTowersRegion[j][i+12].data    = NegRCTTwr3PF[i].towers[j].data;
        GCTTowersRegion[j][i+16].data    = NegRCTTwr4PF[i].towers[j].data;
    }
}

for(idx i=0; i<N_FIBERS_RCT; i++){
    for(idx j=0; j<(N_GCT_ETA/2); j++){
                #pragma HLS unroll
        GCTTowersRegion[j+17][i].data    = PosRCTTwr0PF[i].towers[j].data;
        GCTTowersRegion[j+17][i+4].data  = PosRCTTwr1PF[i].towers[j].data;
        GCTTowersRegion[j+17][i+8].data  = PosRCTTwr2PF[i].towers[j].data;
        GCTTowersRegion[j+17][i+12].data = PosRCTTwr3PF[i].towers[j].data;
        GCTTowersRegion[j+17][i+16].data = PosRCTTwr4PF[i].towers[j].data;
    }
}

}

void makeCorrelatorPayload(GCTClustersInPhi ClsCardIn[N_FIBERS_RCT], GCTClustersInPhi ClsCardOut[N_FIBERS_RCT], ap_uint<1> regInEta){
#pragma HLS ARRAY_PARTITION variable=ClsCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=ClsCardOut complete dim=0
#pragma HLS INLINE

if(regInEta == 0){
    for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){

            ap_uint<12> et       = ClsCardIn[i].GCTclusters[j].et();
            ap_uint<1> towEtaNeg = ClsCardIn[i].GCTclusters[j].towEtaNeg();
            ap_uint<6> towerEta  = ClsCardIn[i].GCTclusters[j].towerEta() + N_GCT_ETA/2;
            ap_uint<5> towerPhi  = (ClsCardIn[i].GCTclusters[j].towerPhi() - 4);
            ap_uint<3> crEta     = ClsCardIn[i].GCTclusters[j].crEta();
            ap_uint<3> crPhi     = ClsCardIn[i].GCTclusters[j].crPhi();
            ap_uint<3> satur     = ClsCardIn[i].GCTclusters[j].satur();
            ap_uint<3> hoe       = 0;
            ap_uint<3> iso       = 0;
            ap_uint<3> shape     = 0;
            ap_uint<2> brems     = ClsCardIn[i].GCTclusters[j].brems();

            ap_uint<44> data = ((ap_uint<44>) et) |
                               (((ap_uint<44>) towEtaNeg)  << 12) |
                               (((ap_uint<44>) towerEta)   << 13) |
                               (((ap_uint<44>) towerPhi)   << 19) |
                               (((ap_uint<44>) crEta)      << 24) |
                               (((ap_uint<44>) crPhi)      << 27) |
                               (((ap_uint<44>) satur)      << 30) |
                               (((ap_uint<44>) hoe)        << 33) |
                               (((ap_uint<44>) iso)        << 36) |
                               (((ap_uint<44>) shape)      << 39) |
                               (((ap_uint<44>) brems)      << 42);

            ClsCardOut[i].GCTclusters[j].data = data;
        }
    }
}

if(regInEta == 1){
    for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){

            ap_uint<12> et       = ClsCardIn[i].GCTclusters[j].et();
            ap_uint<1> towEtaNeg = ClsCardIn[i].GCTclusters[j].towEtaNeg();
            ap_uint<6> towerEta  = N_GCT_ETA/2 - 1 - ClsCardIn[i].GCTclusters[j].towerEta();
            ap_uint<5> towerPhi  = (ClsCardIn[i].GCTclusters[j].towerPhi() - 4);
            ap_uint<3> crEta     = ClsCardIn[i].GCTclusters[j].crEta();
            ap_uint<3> crPhi     = ClsCardIn[i].GCTclusters[j].crPhi();
            ap_uint<3> satur     = ClsCardIn[i].GCTclusters[j].satur();
            ap_uint<3> hoe       = 0;
            ap_uint<3> iso       = 0;
            ap_uint<3> shape     = 0;
            ap_uint<2> brems     = ClsCardIn[i].GCTclusters[j].brems();

            ap_uint<44> data = ((ap_uint<44>) et) |
                                   (((ap_uint<44>) towEtaNeg)  << 12) |
                                   (((ap_uint<44>) towerEta)   << 13) |
                                   (((ap_uint<44>) towerPhi)   << 19) |
                                   (((ap_uint<44>) crEta)      << 24) |
                                   (((ap_uint<44>) crPhi)      << 27) |
                                   (((ap_uint<44>) satur)      << 30) |
                                   (((ap_uint<44>) hoe)        << 33) |
                                   (((ap_uint<44>) iso)        << 36) |
                                   (((ap_uint<44>) shape)      << 39) |
                                   (((ap_uint<44>) brems)      << 42);

            ClsCardOut[i].GCTclusters[j].data = data;
        }
    }
}

}

void makeFullTowerPos(TowersInPhi TwrCardIn[N_FIBERS_RCT], GCTClustersInPhi ClsCardIn[N_FIBERS_RCT], TowersInPhi TwrCardOut[N_FIBERS_RCT], ap_uint<5> regPhi){
#pragma HLS ARRAY_PARTITION variable=TwrCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=ClsCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=TwrCardOut complete dim=0
#pragma HLS PIPELINE

TowersInPhi InterTower[N_FIBERS_RCT];
TowersInPhi InterTowerD0[N_FIBERS_RCT];
#pragma HLS ARRAY_PARTITION variable=InterTower complete dim=0
#pragma HLS ARRAY_PARTITION variable=InterTowerD0 complete dim=0

for(idx crx = 0; crx<N_FIBERS_RCT; crx++){
    for(idx cry = 0; cry<N_RCTCLUSTERS_FIBER; cry++){
        ap_uint<6>  crEta = ClsCardIn[crx].GCTclusters[cry].towerEta();
        ap_uint<5>  crPhi = ClsCardIn[crx].GCTclusters[cry].towerPhi();
        ap_uint<12> crEt  = ClsCardIn[crx].GCTclusters[cry].et();
        for(idx twrx=0; twrx<N_FIBERS_RCT; twrx++){
                for(idx twry=0; twry<N_RCTTOWERS_FIBER; twry++){
                        ap_uint<4> hoe = 0;
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) crEt);
                        bool MatchEtaPhi = ((twrx+4*regPhi) == crPhi && twry == crEta);
                        if(MatchEtaPhi == 1) InterTower[twrx].towers[twry].data = data;
                }
        }
    }
}


for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                TowerBuffer(InterTower[i].towers[j], InterTowerD0[i].towers[j]);
        }
}

for(idx twrx = 0; twrx<N_FIBERS_RCT; twrx++){
        for(idx twry = 0; twry<N_RCTTOWERS_FIBER; twry++){
                ap_uint<12> MergedtwrEt = InterTowerD0[twrx].towers[twry].et() + TwrCardIn[twrx].towers[twry].et();
                ap_uint<4> hoe = 0;
                ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) MergedtwrEt);
                TwrCardOut[twrx].towers[twry].data = data;
        }
}

}

void makeFullTowerNeg(TowersInPhi TwrCardIn[N_FIBERS_RCT], GCTClustersInPhi ClsCardIn[N_FIBERS_RCT], TowersInPhi TwrCardOut[N_FIBERS_RCT], ap_uint<5> regPhi){
#pragma HLS ARRAY_PARTITION variable=TwrCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=ClsCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=TwrCardOut complete dim=0
#pragma HLS PIPELINE

TowersInPhi InterTower[N_FIBERS_RCT];
TowersInPhi InterTowerD0[N_FIBERS_RCT];
#pragma HLS ARRAY_PARTITION variable=InterTower complete dim=0
#pragma HLS ARRAY_PARTITION variable=InterTowerD0 complete dim=0

for(idx crx = 0; crx<N_FIBERS_RCT; crx++){
    for(idx cry = 0; cry<N_RCTCLUSTERS_FIBER; cry++){
        ap_uint<6>  crEta = ClsCardIn[crx].GCTclusters[cry].towerEta();
        ap_uint<5>  crPhi = ClsCardIn[crx].GCTclusters[cry].towerPhi();
        ap_uint<12> crEt  = ClsCardIn[crx].GCTclusters[cry].et();
        for(idx twrx=0; twrx<N_FIBERS_RCT; twrx++){
                for(idx twry=0; twry<N_RCTTOWERS_FIBER; twry++){
                        ap_uint<4> hoe = 0;
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) crEt);
                        bool MatchEtaPhi = ((twrx+4*regPhi) == crPhi && twry == crEta);
                        if(MatchEtaPhi == 1) InterTower[twrx].towers[twry].data = data;
                }
        }
    }
}

for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                TowerBuffer(InterTower[i].towers[j], InterTowerD0[i].towers[j]);
        }
}

for(idx twrx = 0; twrx<N_FIBERS_RCT; twrx++){
        for(idx twry = 0; twry<N_RCTTOWERS_FIBER; twry++){
                ap_uint<12> MergedtwrEt = InterTowerD0[twrx].towers[twry].et() + TwrCardIn[twrx].towers[twry].et();
                ap_uint<4> hoe = 0;
                ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) MergedtwrEt);
                TwrCardOut[twrx].towers[N_RCTTOWERS_FIBER-1-twry].data = data;
        }
}

}

void makeGCTRegions(TowersInPhi TwrCardIn[N_FIBERS_RCT], ClustersInPhi ClsCardIn[N_FIBERS_RCT], TowersInPhi TwrCardOut[N_FIBERS_RCT], GCTClustersInPhi ClsCardOut[N_FIBERS_RCT], ap_uint<3> regInPhi, ap_uint<1> regInEta){
#pragma HLS ARRAY_PARTITION variable=TwrCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=ClsCardIn complete dim=0
#pragma HLS ARRAY_PARTITION variable=TwrCardOut complete dim=0
#pragma HLS ARRAY_PARTITION variable=ClsCardOut complete dim=0
#pragma HLS INLINE

if(regInEta == 0){
    if(regInPhi == 0){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                    ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                    ClsCardOut[i].GCTclusters[j].data = data;
                }
            }
    }
    if(regInPhi == 1){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                    ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                    ClsCardOut[i].GCTclusters[j].data = data;
                }
            }

    }
    if(regInPhi == 2){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[i].GCTclusters[j].data = data;
                }
            }

    }
    if(regInPhi == 3){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();
                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[i].GCTclusters[j].data = data;
                }
            }

    }
    if(regInPhi == 4){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();
                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[i].GCTclusters[j].data = data;
                }
            }

    }
}

if(regInEta == 1){
    if(regInPhi == 0){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[3-i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = 3-ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = 4-ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[3-i].GCTclusters[j].data = data;
                }
            }
    }
    if(regInPhi == 1){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[3-i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = 3-ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = 4-ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[3-i].GCTclusters[j].data = data;
                }
            }
    }
    if(regInPhi == 2){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[3-i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = 3-ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = 4-ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[3-i].GCTclusters[j].data = data;
                }
            }
    }
    if(regInPhi == 3){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                #pragma HLS unroll
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[3-i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = 3-ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                        ap_uint<3> crPhi     = 4-ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[3-i].GCTclusters[j].data = data;
                }
            }
    }
    if(regInPhi == 4){
        /*-------------------TOWER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTTOWERS_FIBER; j++){ /*-0 => 16-*/
                    #pragma HLS unroll
                        ap_uint<4> hoe = TwrCardIn[i].towers[j].hoe();
                        ap_uint<12> et = TwrCardIn[i].towers[j].et();
                        ap_uint<16> data = ((ap_uint<16>) hoe << 12) | ((ap_uint<16>) et);
                    TwrCardOut[3-i].towers[j].data = data;
                }
            }
        /*------------------CLUSTER REGION------------------*/
            for(idx i=0; i<N_FIBERS_RCT; i++){ /*-0 => 3-*/
                for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){ /*-0 => 1-*/
                    #pragma HLS unroll
                        ap_uint<12> et       = ClsCardIn[i].RCTclusters[j].clusterEnergy();
                        ap_uint<1> towEtaNeg = regInEta;
                        ap_uint<6> towerEta  = ClsCardIn[i].RCTclusters[j].towerEta();
                        ap_uint<5> towerPhi  = 3-ClsCardIn[i].RCTclusters[j].towerPhi() + regInPhi*4;
                        ap_uint<3> crEta     = ClsCardIn[i].RCTclusters[j].clusterEta();
                    ap_uint<3> crPhi     = 4-ClsCardIn[i].RCTclusters[j].clusterPhi();
                        ap_uint<3> satur     = ClsCardIn[i].RCTclusters[j].satur();
                        ap_uint<3> hoe       = 0;
                        ap_uint<3> iso       = 0;
                        ap_uint<3> shape     = 0;
                        ap_uint<2> brems     = ClsCardIn[i].RCTclusters[j].brems();

                        ap_uint<44> data = ((ap_uint<44>) et) |
                                           (((ap_uint<44>) towEtaNeg)  << 12) |
                                           (((ap_uint<44>) towerEta)   << 13) |
                                           (((ap_uint<44>) towerPhi)   << 19) |
                                           (((ap_uint<44>) crEta)      << 24) |
                                           (((ap_uint<44>) crPhi)      << 27) |
                                           (((ap_uint<44>) satur)      << 30) |
                                           (((ap_uint<44>) hoe)        << 33) |
                                           (((ap_uint<44>) iso)        << 36) |
                                           (((ap_uint<44>) shape)      << 39) |
                                           (((ap_uint<44>) brems)      << 42);

                        ClsCardOut[3-i].GCTclusters[j].data = data;
                }
            }
    }
}

}

void stitchClsPhiPosEta(RCTClusterPrimitives ClusterIn1, RCTClusterPrimitives ClusterIn2, RCTClusterPrimitives &OutStchCluster1, RCTClusterPrimitives &OutStchCluster2){
#pragma HLS INLINE

ap_uint<8> eta1 = ClusterIn1.towerEta()*5  + ClusterIn1.clusterEta();
ap_uint<8> eta2 = ClusterIn2.towerEta()*5  + ClusterIn2.clusterEta();
ap_uint<8> dEta;

RCTClusterPrimitives OutStchCluster1Reg = OutStchCluster1;
RCTClusterPrimitives OutStchCluster2Reg = OutStchCluster2;

dEta = (eta1>eta2) ? (eta1-eta2):(eta2-eta1);

bool ClsInTwrPhiLevel1      = ((ClusterIn2.towerPhi()+3) == ClusterIn1.towerPhi());
bool ClsInTwrPhiLevel2      = (ClusterIn2.clusterPhi() == 0 && ClusterIn1.clusterPhi() == 4);
bool ShrSameEta             = (dEta<2);

bool stitch                 = (ClsInTwrPhiLevel1 && ClsInTwrPhiLevel2 && ShrSameEta);

ap_uint<13> cEtSum          = ClusterIn1.clusterEnergy() + ClusterIn2.clusterEnergy();

if(stitch == 1){
    if(ClusterIn1.clusterEnergy() > ClusterIn2.clusterEnergy()){
                OutStchCluster1 = RCTClusterPrimitives(cEtSum, ClusterIn1.towerEta(), ClusterIn1.towerPhi(), ClusterIn1.clusterEta(), ClusterIn1.clusterPhi(), ClusterIn1.satur(), 0, 0, ClusterIn1.brems());
                OutStchCluster2 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
            else{
                OutStchCluster1 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
                OutStchCluster2 = RCTClusterPrimitives(cEtSum, ClusterIn2.towerEta(), ClusterIn2.towerPhi(), ClusterIn2.clusterEta(), ClusterIn2.clusterPhi(), ClusterIn2.satur(), 0, 0, ClusterIn2.brems());
        }
}
else{
        OutStchCluster1 = OutStchCluster1Reg;
        OutStchCluster2 = OutStchCluster2Reg;
}
}

void stitchClsPhiNegEta(RCTClusterPrimitives ClusterIn1, RCTClusterPrimitives ClusterIn2, RCTClusterPrimitives &OutStchCluster1, RCTClusterPrimitives &OutStchCluster2){
#pragma HLS INLINE

ap_uint<8> eta1 = ClusterIn1.towerEta()*5  + ClusterIn1.clusterEta();
ap_uint<8> eta2 = ClusterIn2.towerEta()*5  + ClusterIn2.clusterEta();
ap_uint<8> dEta;

RCTClusterPrimitives OutStchCluster1Reg = OutStchCluster1;
RCTClusterPrimitives OutStchCluster2Reg = OutStchCluster2;

dEta = (eta1>eta2) ? (eta1-eta2):(eta2-eta1);

bool ClsInTwrPhiLevel1      = (ClusterIn2.towerPhi() == (ClusterIn1.towerPhi() + 3));
bool ClsInTwrPhiLevel2      = (ClusterIn2.clusterPhi() == 4 && ClusterIn1.clusterPhi() == 0);
bool ShrSameEta             = (dEta<2);

bool stitch                 = (ClsInTwrPhiLevel1 && ClsInTwrPhiLevel2 && ShrSameEta);

ap_uint<12> cEtSum          = ClusterIn1.clusterEnergy() + ClusterIn2.clusterEnergy();

if(stitch == 1){
    if(ClusterIn1.clusterEnergy() > ClusterIn2.clusterEnergy()){
                OutStchCluster1 = RCTClusterPrimitives(cEtSum, ClusterIn1.towerEta(), ClusterIn1.towerPhi(), ClusterIn1.clusterEta(), ClusterIn1.clusterPhi(), ClusterIn1.satur(), 0, 0, ClusterIn1.brems());
                OutStchCluster2 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
            else{
                OutStchCluster1 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
                OutStchCluster2 = RCTClusterPrimitives(cEtSum, ClusterIn2.towerEta(), ClusterIn2.towerPhi(), ClusterIn2.clusterEta(), ClusterIn2.clusterPhi(), ClusterIn2.satur(), 0, 0, ClusterIn2.brems());
        }
}
else{
        OutStchCluster1 = OutStchCluster1Reg;
        OutStchCluster2 = OutStchCluster2Reg;
}
}

void stitchClsEta(RCTClusterPrimitives ClusterIn1, RCTClusterPrimitives ClusterIn2, RCTClusterPrimitives &OutStchCluster1, RCTClusterPrimitives &OutStchCluster2){
#pragma HLS INLINE

ap_uint<5> phi1    = ClusterIn1.towerPhi()*5  + ClusterIn1.clusterPhi();
ap_uint<5> phi2    = 19 - (ClusterIn2.towerPhi()*5  + ClusterIn2.clusterPhi());
ap_uint<5> dPhi;

RCTClusterPrimitives OutStchCluster1Reg = OutStchCluster1;
RCTClusterPrimitives OutStchCluster2Reg = OutStchCluster2;

dPhi = (phi1>phi2)?(phi1-phi2):(phi2-phi1);

bool ClsInTwrEtaLevel1      = ((ClusterIn2.towerEta() == 0) && (ClusterIn1.towerEta() == 0));
bool ClsInTwrEtaLevel2      = (ClusterIn2.clusterEta() == 0 && ClusterIn1.clusterEta() == 0);
bool ShrSamePhi1            = (dPhi<2);

bool stitch                 = (ClsInTwrEtaLevel1 && ClsInTwrEtaLevel2 && ShrSamePhi1);

ap_uint<12> cEtSum          = ClusterIn1.clusterEnergy() + ClusterIn2.clusterEnergy();

if(stitch == 1){
    if(ClusterIn1.clusterEnergy() > ClusterIn2.clusterEnergy()){
                OutStchCluster1 = RCTClusterPrimitives(cEtSum, ClusterIn1.towerEta(), ClusterIn1.towerPhi(), ClusterIn1.clusterEta(), ClusterIn1.clusterPhi(), ClusterIn1.satur(), 0, 0, ClusterIn1.brems());
                OutStchCluster2 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
            }
            else{
                OutStchCluster1 = RCTClusterPrimitives(0, 0, 0, 0, 0, 0, 0, 0, 0);
                OutStchCluster2 = RCTClusterPrimitives(cEtSum, ClusterIn2.towerEta(), ClusterIn2.towerPhi(), ClusterIn2.clusterEta(), ClusterIn2.clusterPhi(), ClusterIn2.satur(), 0, 0, ClusterIn2.brems());
        }
}
else{
        OutStchCluster1 = OutStchCluster1Reg;
        OutStchCluster2 = OutStchCluster2Reg;
}
}

void stitchRCTPosPhi(ClustersInPhi inRCT0[N_FIBERS_RCT], ClustersInPhi inRCT1[N_FIBERS_RCT], ClustersInPhi outRCT0[N_FIBERS_RCT], ClustersInPhi outRCT1[N_FIBERS_RCT]){
#pragma HLS ARRAY_PARTITION variable=inRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=inRCT1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT1 complete dim=0
#pragma HLS PIPELINE

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){ /*-0 => 3-*/
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){ /*-0 => 2-*/
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[0], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[0].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[0], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[1].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[0], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[2].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[0], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[3].RCTclusters[clsItr]);
    }
}

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){ /*-0 => 3-*/
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){ /*-0 => 2-*/
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[1], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[0].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[1], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[1].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[1], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[2].RCTclusters[clsItr]);
        stitchClsPhiPosEta(inRCT0[inPhi].RCTclusters[1], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[3].RCTclusters[clsItr]);
    }
}

}

void stitchRCTNegPhi(ClustersInPhi inRCT0[N_FIBERS_RCT], ClustersInPhi inRCT1[N_FIBERS_RCT], ClustersInPhi outRCT0[N_FIBERS_RCT], ClustersInPhi outRCT1[N_FIBERS_RCT]){
#pragma HLS ARRAY_PARTITION variable=inRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=inRCT1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT1 complete dim=0
#pragma HLS PIPELINE

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){ /*-0 => 3-*/
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){ /*-0 => 2-*/
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[0], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[0].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[0], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[1].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[0], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[2].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[0], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[3].RCTclusters[clsItr]);
    }
}

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){ /*-0 => 3-*/
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){ /*-0 => 2-*/
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[1], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[0].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[1], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[1].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[1], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[2].RCTclusters[clsItr]);
        stitchClsPhiNegEta(inRCT0[inPhi].RCTclusters[1], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[3].RCTclusters[clsItr]);
    }
}

}

void stitchRCTEta(ClustersInPhi inRCT0[N_FIBERS_RCT], ClustersInPhi inRCT1[N_FIBERS_RCT], ClustersInPhi outRCT0[N_FIBERS_RCT], ClustersInPhi outRCT1[N_FIBERS_RCT]){
#pragma HLS ARRAY_PARTITION variable=inRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=inRCT1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=outRCT1 complete dim=0
#pragma HLS PIPELINE

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){
        stitchClsEta(inRCT0[inPhi].RCTclusters[0], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[0].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[0], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[1].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[0], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[2].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[0], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[0], outRCT1[3].RCTclusters[clsItr]);
    }
}

for(idx inPhi = 0; inPhi<N_FIBERS_RCT; inPhi++){
    for(idx clsItr = 0; clsItr<N_RCTCLUSTERS_FIBER; clsItr++){
        stitchClsEta(inRCT0[inPhi].RCTclusters[1], inRCT1[0].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[0].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[1], inRCT1[1].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[1].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[1], inRCT1[2].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[2].RCTclusters[clsItr]);
        stitchClsEta(inRCT0[inPhi].RCTclusters[1], inRCT1[3].RCTclusters[clsItr], outRCT0[inPhi].RCTclusters[1], outRCT1[3].RCTclusters[clsItr]);
    }
}

}

void processInLinks(ap_uint<576> link_in_reg[N_FIBERS_RCT], TowersInPhi RCTTwr[N_FIBERS_RCT], ClustersInPhi RCTCls[N_FIBERS_RCT]){
#pragma HLS ARRAY_PARTITION variable=link_in_reg complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTTwr complete dim=0
#pragma HLS ARRAY_PARTITION variable=RCTCls complete dim=0
#pragma HLS PIPELINE
#pragma HLS LATENCY min=1

for(idx phi=0; phi<N_FIBERS_RCT; phi++){  /*0 => 3 (4 phi strip)*/
    idx start = 0;
    for(idx twr=0; twr<N_RCTTOWERS_FIBER; twr++){  /*0 => 16 (17 towers in eta)*/
        #pragma HLS unroll
        RCTTwr[phi].towers[twr].data = link_in_reg[phi].range(start+15, start);
        start = start + 16;
    }
}

for(idx phi=0; phi<N_FIBERS_RCT; phi++){ /*0 => 3 (4 phi strip)*/
    idx start = 272; /*16b x 17 Towers = 272*/
    for(idx cls=0; cls<N_RCTCLUSTERS_FIBER; cls++){ /*0 => 1 (2 clusters per phi)*/
        #pragma HLS unroll
        RCTCls[phi].RCTclusters[cls].data = link_in_reg[phi].range(start+59, start);
        start = start + 60;
    }
}

}

void algo_top(ap_uint<576> link_in[N_INPUT_LINKS], ap_uint<576> link_out_CORR[N_OUTPUT_LINKS], ap_uint<576> link_out_PF[N_OUTPUT_LINKS], ap_uint<576> link_out_JETS[N_OUTPUT_LINKS_JETS]){
#pragma HLS ARRAY_PARTITION variable=link_in complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_out_CORR complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_out_PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_out_JETS complete dim=0

#pragma HLS INTERFACE ap_ctrl_hs port=return
#pragma HLS PIPELINE II=9

/*-------5 POSITIVE ETA RCT CARDS---------*/

TowersInPhi PosRCTTwr0[N_FIBERS_RCT];
TowersInPhi PosRCTTwr1[N_FIBERS_RCT];
TowersInPhi PosRCTTwr2[N_FIBERS_RCT];
TowersInPhi PosRCTTwr3[N_FIBERS_RCT];
TowersInPhi PosRCTTwr4[N_FIBERS_RCT];

ClustersInPhi PosRCTCls0[N_FIBERS_RCT];
ClustersInPhi PosRCTCls1[N_FIBERS_RCT];
ClustersInPhi PosRCTCls2[N_FIBERS_RCT];
ClustersInPhi PosRCTCls3[N_FIBERS_RCT];
ClustersInPhi PosRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=PosRCTTwr0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr4 complete dim=0

#pragma HLS ARRAY_PARTITION variable=PosRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls4 complete dim=0

/*-------5 NEGATIVE ETA RCT CARDS---------*/

TowersInPhi NegRCTTwr0[N_FIBERS_RCT];
TowersInPhi NegRCTTwr1[N_FIBERS_RCT];
TowersInPhi NegRCTTwr2[N_FIBERS_RCT];
TowersInPhi NegRCTTwr3[N_FIBERS_RCT];
TowersInPhi NegRCTTwr4[N_FIBERS_RCT];

ClustersInPhi NegRCTCls0[N_FIBERS_RCT];
ClustersInPhi NegRCTCls1[N_FIBERS_RCT];
ClustersInPhi NegRCTCls2[N_FIBERS_RCT];
ClustersInPhi NegRCTCls3[N_FIBERS_RCT];
ClustersInPhi NegRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=NegRCTTwr0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr4 complete dim=0

#pragma HLS ARRAY_PARTITION variable=NegRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls4 complete dim=0

/*------------------------------------------------------------------*/

ap_uint<576> link_in_pos_0[N_FIBERS_RCT];
ap_uint<576> link_in_pos_1[N_FIBERS_RCT];
ap_uint<576> link_in_pos_2[N_FIBERS_RCT];
ap_uint<576> link_in_pos_3[N_FIBERS_RCT];
ap_uint<576> link_in_pos_4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=link_in_pos_0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_pos_1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_pos_2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_pos_3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_pos_4 complete dim=0

ap_uint<576> link_in_neg_0[N_FIBERS_RCT];
ap_uint<576> link_in_neg_1[N_FIBERS_RCT];
ap_uint<576> link_in_neg_2[N_FIBERS_RCT];
ap_uint<576> link_in_neg_3[N_FIBERS_RCT];
ap_uint<576> link_in_neg_4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=link_in_neg_0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_neg_1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_neg_2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_neg_3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=link_in_neg_4 complete dim=0

for(idx i=0; i<N_FIBERS_RCT; i++){
    #pragma HLS unroll
    link_in_pos_0[i] = link_in[0+i];
    link_in_pos_1[i] = link_in[4+i];
    link_in_pos_2[i] = link_in[8+i];
    link_in_pos_3[i] = link_in[12+i];
    link_in_pos_4[i] = link_in[16+i];
}

for(idx i=0; i<N_FIBERS_RCT; i++){
    #pragma HLS unroll
    link_in_neg_0[i] = link_in[20+i];
    link_in_neg_1[i] = link_in[24+i];
    link_in_neg_2[i] = link_in[28+i];
    link_in_neg_3[i] = link_in[32+i];
    link_in_neg_4[i] = link_in[36+i];
}

processInLinks(link_in_pos_0, PosRCTTwr0, PosRCTCls0);
processInLinks(link_in_pos_1, PosRCTTwr1, PosRCTCls1);
processInLinks(link_in_pos_2, PosRCTTwr2, PosRCTCls2);
processInLinks(link_in_pos_3, PosRCTTwr3, PosRCTCls3);
processInLinks(link_in_pos_4, PosRCTTwr4, PosRCTCls4);

processInLinks(link_in_neg_0, NegRCTTwr0, NegRCTCls0);
processInLinks(link_in_neg_1, NegRCTTwr1, NegRCTCls1);
processInLinks(link_in_neg_2, NegRCTTwr2, NegRCTCls2);
processInLinks(link_in_neg_3, NegRCTTwr3, NegRCTCls3);
processInLinks(link_in_neg_4, NegRCTTwr4, NegRCTCls4);

/*-----------------------STITCHING OF CLUSTERS------------------------*/

/*----------PASSING THE CLUSTERS INFORMATION TO THE OUTPUT------------*/

ClustersInPhi StchLv1PosRCTCls0[N_FIBERS_RCT];
ClustersInPhi StchLv1PosRCTCls1[N_FIBERS_RCT];
ClustersInPhi StchLv1PosRCTCls2[N_FIBERS_RCT];
ClustersInPhi StchLv1PosRCTCls3[N_FIBERS_RCT];
ClustersInPhi StchLv1PosRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=StchLv1PosRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1PosRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1PosRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1PosRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1PosRCTCls4 complete dim=0

ClustersInPhi StchLv1NegRCTCls0[N_FIBERS_RCT];
ClustersInPhi StchLv1NegRCTCls1[N_FIBERS_RCT];
ClustersInPhi StchLv1NegRCTCls2[N_FIBERS_RCT];
ClustersInPhi StchLv1NegRCTCls3[N_FIBERS_RCT];
ClustersInPhi StchLv1NegRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=StchLv1NegRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1NegRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1NegRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1NegRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv1NegRCTCls4 complete dim=0

for(idx i=0; i<N_FIBERS_RCT; i++){ /*0 => 3 (4 phi strip)*/
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){
                #pragma HLS unroll
            StchLv1PosRCTCls0[i].RCTclusters[j].data = PosRCTCls0[i].RCTclusters[j].data;
            StchLv1PosRCTCls1[i].RCTclusters[j].data = PosRCTCls1[i].RCTclusters[j].data;
            StchLv1PosRCTCls2[i].RCTclusters[j].data = PosRCTCls2[i].RCTclusters[j].data;
            StchLv1PosRCTCls3[i].RCTclusters[j].data = PosRCTCls3[i].RCTclusters[j].data;
            StchLv1PosRCTCls4[i].RCTclusters[j].data = PosRCTCls4[i].RCTclusters[j].data;
        }
}

for(idx i=0; i<N_FIBERS_RCT; i++){ /*0 => 3 (4 phi strip)*/
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){
                #pragma HLS unroll
                StchLv1NegRCTCls0[i].RCTclusters[j].data = NegRCTCls0[i].RCTclusters[j].data;
                StchLv1NegRCTCls1[i].RCTclusters[j].data = NegRCTCls1[i].RCTclusters[j].data;
                StchLv1NegRCTCls2[i].RCTclusters[j].data = NegRCTCls2[i].RCTclusters[j].data;
                StchLv1NegRCTCls3[i].RCTclusters[j].data = NegRCTCls3[i].RCTclusters[j].data;
                StchLv1NegRCTCls4[i].RCTclusters[j].data = NegRCTCls4[i].RCTclusters[j].data;
        }
}

/*---------------------NOW STITCHING IN PHI => EVEN CARDS----------------------*/

stitchRCTPosPhi(PosRCTCls0, PosRCTCls1, StchLv1PosRCTCls0, StchLv1PosRCTCls1);
stitchRCTPosPhi(PosRCTCls2, PosRCTCls3, StchLv1PosRCTCls2, StchLv1PosRCTCls3);

stitchRCTNegPhi(NegRCTCls0, NegRCTCls1, StchLv1NegRCTCls0, StchLv1NegRCTCls1);
stitchRCTNegPhi(NegRCTCls2, NegRCTCls3, StchLv1NegRCTCls2, StchLv1NegRCTCls3);

/*---------------------NOW STITCHING IN PHI => ODD CARDS-----------------------*/

stitchRCTPosPhi(PosRCTCls1, PosRCTCls2, StchLv1PosRCTCls1, StchLv1PosRCTCls2);
stitchRCTPosPhi(PosRCTCls3, PosRCTCls4, StchLv1PosRCTCls3, StchLv1PosRCTCls4);

stitchRCTNegPhi(NegRCTCls1, NegRCTCls2, StchLv1NegRCTCls1, StchLv1NegRCTCls2);
stitchRCTNegPhi(NegRCTCls3, NegRCTCls4, StchLv1NegRCTCls3, StchLv1NegRCTCls4);

/*---------------------------NOW STITCHING IN ETA------------------------------*/

ClustersInPhi StchLv2PosRCTCls0[N_FIBERS_RCT];
ClustersInPhi StchLv2PosRCTCls1[N_FIBERS_RCT];
ClustersInPhi StchLv2PosRCTCls2[N_FIBERS_RCT];
ClustersInPhi StchLv2PosRCTCls3[N_FIBERS_RCT];
ClustersInPhi StchLv2PosRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=StchLv2PosRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2PosRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2PosRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2PosRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2PosRCTCls4 complete dim=0

ClustersInPhi StchLv2NegRCTCls0[N_FIBERS_RCT];
ClustersInPhi StchLv2NegRCTCls1[N_FIBERS_RCT];
ClustersInPhi StchLv2NegRCTCls2[N_FIBERS_RCT];
ClustersInPhi StchLv2NegRCTCls3[N_FIBERS_RCT];
ClustersInPhi StchLv2NegRCTCls4[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=StchLv2NegRCTCls0 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2NegRCTCls1 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2NegRCTCls2 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2NegRCTCls3 complete dim=0
#pragma HLS ARRAY_PARTITION variable=StchLv2NegRCTCls4 complete dim=0

for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){
                #pragma HLS unroll
                StchLv2PosRCTCls0[i].RCTclusters[j].data = StchLv1PosRCTCls0[i].RCTclusters[j].data;
                StchLv2PosRCTCls1[i].RCTclusters[j].data = StchLv1PosRCTCls1[i].RCTclusters[j].data;
                StchLv2PosRCTCls2[i].RCTclusters[j].data = StchLv1PosRCTCls2[i].RCTclusters[j].data;
                StchLv2PosRCTCls3[i].RCTclusters[j].data = StchLv1PosRCTCls3[i].RCTclusters[j].data;
                StchLv2PosRCTCls4[i].RCTclusters[j].data = StchLv1PosRCTCls4[i].RCTclusters[j].data;
        }
}

for(idx i=0; i<N_FIBERS_RCT; i++){
        for(idx j=0; j<N_RCTCLUSTERS_FIBER; j++){
                #pragma HLS unroll
                StchLv2NegRCTCls0[i].RCTclusters[j].data = StchLv1NegRCTCls0[i].RCTclusters[j].data;
                StchLv2NegRCTCls1[i].RCTclusters[j].data = StchLv1NegRCTCls1[i].RCTclusters[j].data;
                StchLv2NegRCTCls2[i].RCTclusters[j].data = StchLv1NegRCTCls2[i].RCTclusters[j].data;
                StchLv2NegRCTCls3[i].RCTclusters[j].data = StchLv1NegRCTCls3[i].RCTclusters[j].data;
                StchLv2NegRCTCls4[i].RCTclusters[j].data = StchLv1NegRCTCls4[i].RCTclusters[j].data;
        }
}

stitchRCTEta(StchLv1PosRCTCls0, StchLv1NegRCTCls0, StchLv2PosRCTCls0, StchLv2NegRCTCls0);
stitchRCTEta(StchLv1PosRCTCls1, StchLv1NegRCTCls1, StchLv2PosRCTCls1, StchLv2NegRCTCls1);
stitchRCTEta(StchLv1PosRCTCls2, StchLv1NegRCTCls2, StchLv2PosRCTCls2, StchLv2NegRCTCls2);
stitchRCTEta(StchLv1PosRCTCls3, StchLv1NegRCTCls3, StchLv2PosRCTCls3, StchLv2NegRCTCls3);
stitchRCTEta(StchLv1PosRCTCls4, StchLv1NegRCTCls4, StchLv2PosRCTCls4, StchLv2NegRCTCls4);

/*---------------CREATING GCT INTERNAL REGIONS ---------------*/

TowersInPhi PosRCTTwr0GCT[N_FIBERS_RCT];
TowersInPhi PosRCTTwr1GCT[N_FIBERS_RCT];
TowersInPhi PosRCTTwr2GCT[N_FIBERS_RCT];
TowersInPhi PosRCTTwr3GCT[N_FIBERS_RCT];
TowersInPhi PosRCTTwr4GCT[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=PosRCTTwr0GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr1GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr2GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr3GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr4GCT complete dim=0

TowersInPhi NegRCTTwr0GCT[N_FIBERS_RCT];
TowersInPhi NegRCTTwr1GCT[N_FIBERS_RCT];
TowersInPhi NegRCTTwr2GCT[N_FIBERS_RCT];
TowersInPhi NegRCTTwr3GCT[N_FIBERS_RCT];
TowersInPhi NegRCTTwr4GCT[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=NegRCTTwr0GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr1GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr2GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr3GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr4GCT complete dim=0

GCTClustersInPhi PosRCTCls0GCT[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls1GCT[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls2GCT[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls3GCT[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls4GCT[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=PosRCTCls0GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls1GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls2GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls3GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls4GCT complete dim=0

GCTClustersInPhi NegRCTCls0GCT[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls1GCT[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls2GCT[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls3GCT[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls4GCT[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=NegRCTCls0GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls1GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls2GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls3GCT complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls4GCT complete dim=0

makeGCTRegions(PosRCTTwr0, StchLv2PosRCTCls0, PosRCTTwr0GCT, PosRCTCls0GCT, 0, 0);
makeGCTRegions(PosRCTTwr1, StchLv2PosRCTCls1, PosRCTTwr1GCT, PosRCTCls1GCT, 1, 0);
makeGCTRegions(PosRCTTwr2, StchLv2PosRCTCls2, PosRCTTwr2GCT, PosRCTCls2GCT, 2, 0);
makeGCTRegions(PosRCTTwr3, StchLv2PosRCTCls3, PosRCTTwr3GCT, PosRCTCls3GCT, 3, 0);
makeGCTRegions(PosRCTTwr4, StchLv2PosRCTCls4, PosRCTTwr4GCT, PosRCTCls4GCT, 4, 0);

makeGCTRegions(NegRCTTwr0, StchLv2NegRCTCls0, NegRCTTwr0GCT, NegRCTCls0GCT, 0, 1);
makeGCTRegions(NegRCTTwr1, StchLv2NegRCTCls1, NegRCTTwr1GCT, NegRCTCls1GCT, 1, 1);
makeGCTRegions(NegRCTTwr2, StchLv2NegRCTCls2, NegRCTTwr2GCT, NegRCTCls2GCT, 2, 1);
makeGCTRegions(NegRCTTwr3, StchLv2NegRCTCls3, NegRCTTwr3GCT, NegRCTCls3GCT, 3, 1);
makeGCTRegions(NegRCTTwr4, StchLv2NegRCTCls4, NegRCTTwr4GCT, NegRCTCls4GCT, 4, 1);

/*----------------------CREATING INFORMATION FOR CORRELATOR--------------------------*/

GCTClustersInPhi PosRCTCls1GCTCorr[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls2GCTCorr[N_FIBERS_RCT];
GCTClustersInPhi PosRCTCls3GCTCorr[N_FIBERS_RCT];

GCTClustersInPhi NegRCTCls1GCTCorr[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls2GCTCorr[N_FIBERS_RCT];
GCTClustersInPhi NegRCTCls3GCTCorr[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=PosRCTCls1GCTCorr complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls2GCTCorr complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTCls3GCTCorr complete dim=0

#pragma HLS ARRAY_PARTITION variable=NegRCTCls1GCTCorr complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls2GCTCorr complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTCls3GCTCorr complete dim=0

makeCorrelatorPayload(PosRCTCls1GCT, PosRCTCls1GCTCorr, 0);
makeCorrelatorPayload(PosRCTCls2GCT, PosRCTCls2GCTCorr, 0);
makeCorrelatorPayload(PosRCTCls3GCT, PosRCTCls3GCTCorr, 0);

makeCorrelatorPayload(NegRCTCls1GCT, NegRCTCls1GCTCorr, 1);
makeCorrelatorPayload(NegRCTCls2GCT, NegRCTCls2GCTCorr, 1);
makeCorrelatorPayload(NegRCTCls3GCT, NegRCTCls3GCTCorr, 1);

/*------------------------------PACKING THE POSITIVE ETA CORRELATOR OUTPUT------------------------------*/

ap_uint<576> link_out_corr_reg[N_OUTPUT_LINKS];
#pragma HLS ARRAY_PARTITION variable=link_out_corr_reg complete dim=0

for(idx i=0; i<4; i++){
        #pragma HLS UNROLL
        link_out_corr_reg[i] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                #pragma HLS UNROLL
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i].range(end_twr, start_twr) = PosRCTTwr1GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
                #pragma HLS UNROLL
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = PosRCTCls1GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

for(idx i=0; i<4; i++){
        #pragma HLS UNROLL
        link_out_corr_reg[4+i] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                #pragma HLS UNROLL
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i+4].range(end_twr, start_twr) = PosRCTTwr2GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
                #pragma HLS UNROLL
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = PosRCTCls2GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i+4].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

for(idx i=0; i<4; i++){
        #pragma HLS UNROLL
        link_out_corr_reg[8+i] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                #pragma HLS UNROLL
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i+8].range(end_twr, start_twr) = PosRCTTwr3GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
                #pragma HLS UNROLL
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = PosRCTCls3GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i+8].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

/*------------------------------PACKING THE NEGATIVE ETA CORRELATOR OUTPUT------------------------------*/

for(idx i=0; i<4; i++){
        #pragma HLS unroll
        link_out_corr_reg[i+12] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
        #pragma HLS unroll
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i+12].range(end_twr, start_twr) = NegRCTTwr1GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
        #pragma HLS unroll
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = NegRCTCls1GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i+12].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

for(idx i=0; i<4; i++){
        #pragma HLS unroll
        link_out_corr_reg[i+16] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                #pragma HLS unroll
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i+16].range(end_twr, start_twr) = NegRCTTwr2GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
                #pragma HLS unroll
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = NegRCTCls2GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i+16].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

for(idx i=0; i<4; i++){
        #pragma HLS unroll
        link_out_corr_reg[i+20] = 0;
    idx start_twr = 0;
    idx start_cls = 272;
    for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
                #pragma HLS unroll
        idx end_twr = start_twr + 15;
        link_out_corr_reg[i+20].range(end_twr, start_twr) = NegRCTTwr3GCT[i].towers[j].data;
        start_twr = start_twr + 16;
    }
    for(idx k=0; k<N_RCTCLUSTERS_FIBER; k++){
                #pragma HLS unroll
        idx end_cls = start_cls + 63;
        ap_uint<64> data = 0;
        data.range(43, 0) = NegRCTCls3GCTCorr[i].GCTclusters[k].data;
        link_out_corr_reg[i+20].range(end_cls, start_cls) = data;
        start_cls = start_cls + 64;
    }
}

prsOutLinksCORR(link_out_corr_reg, link_out_CORR);

/*-----------------------MERGING TOWER AND CLUSTER ENERGY------------------------*/
/*---------------------CREATING INPUT FOR THE PF CLUSTER IP----------------------*/

TowersInPhi PosRCTTwr0PF[N_FIBERS_RCT];
TowersInPhi PosRCTTwr1PF[N_FIBERS_RCT];
TowersInPhi PosRCTTwr2PF[N_FIBERS_RCT];
TowersInPhi PosRCTTwr3PF[N_FIBERS_RCT];
TowersInPhi PosRCTTwr4PF[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=PosRCTTwr0PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr1PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr2PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr3PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=PosRCTTwr4PF complete dim=0

TowersInPhi NegRCTTwr0PF[N_FIBERS_RCT];
TowersInPhi NegRCTTwr1PF[N_FIBERS_RCT];
TowersInPhi NegRCTTwr2PF[N_FIBERS_RCT];
TowersInPhi NegRCTTwr3PF[N_FIBERS_RCT];
TowersInPhi NegRCTTwr4PF[N_FIBERS_RCT];

#pragma HLS ARRAY_PARTITION variable=NegRCTTwr0PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr1PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr2PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr3PF complete dim=0
#pragma HLS ARRAY_PARTITION variable=NegRCTTwr4PF complete dim=0

makeFullTowerPos(PosRCTTwr0GCT, PosRCTCls0GCT, PosRCTTwr0PF, 0);
makeFullTowerPos(PosRCTTwr1GCT, PosRCTCls1GCT, PosRCTTwr1PF, 1);
makeFullTowerPos(PosRCTTwr2GCT, PosRCTCls2GCT, PosRCTTwr2PF, 2);
makeFullTowerPos(PosRCTTwr3GCT, PosRCTCls3GCT, PosRCTTwr3PF, 3);
makeFullTowerPos(PosRCTTwr4GCT, PosRCTCls4GCT, PosRCTTwr4PF, 4);

makeFullTowerNeg(NegRCTTwr0GCT, NegRCTCls0GCT, NegRCTTwr0PF, 0);
makeFullTowerNeg(NegRCTTwr1GCT, NegRCTCls1GCT, NegRCTTwr1PF, 1);
makeFullTowerNeg(NegRCTTwr2GCT, NegRCTCls2GCT, NegRCTTwr2PF, 2);
makeFullTowerNeg(NegRCTTwr3GCT, NegRCTCls3GCT, NegRCTTwr3PF, 3);
makeFullTowerNeg(NegRCTTwr4GCT, NegRCTCls4GCT, NegRCTTwr4PF, 4);

/*----------------------------PACKING THE FULL TOWER FOR JETS------------------------------*/

for(idx i=0; i<N_FIBERS_RCT; i++){
	#pragma HLS unroll
	link_out_JETS[i] = 0;
	idx start_neg = 0;
	idx start_pos = 272;
	for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
		#pragma HLS unroll
        idx end_pos = start_pos + 15;
        idx end_neg = start_neg + 15;
        link_out_JETS[i].range(end_neg, start_neg) = NegRCTTwr0PF[i].towers[j].data;
		link_out_JETS[i].range(end_pos, start_pos) = PosRCTTwr0PF[i].towers[j].data;
		start_pos = start_pos + 16;
		start_neg = start_neg + 16;
	}
}

for(idx i=0; i<N_FIBERS_RCT; i++){
	#pragma HLS unroll
	link_out_JETS[i+4] = 0;
	idx start_neg = 0;
	idx start_pos = 272;
	for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
		#pragma HLS unroll
        idx end_pos = start_pos + 15;
        idx end_neg = start_neg + 15;
        link_out_JETS[i+4].range(end_neg, start_neg) = NegRCTTwr1PF[i].towers[j].data;
        link_out_JETS[i+4].range(end_pos, start_pos) = PosRCTTwr1PF[i].towers[j].data;
		start_pos = start_pos + 16;
		start_neg = start_neg + 16;
	}
}

for(idx i=0; i<N_FIBERS_RCT; i++){
	#pragma HLS unroll
	link_out_JETS[i+8] = 0;
	idx start_neg = 0;
	idx start_pos = 272;
	for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
		#pragma HLS unroll
        idx end_pos = start_pos + 15;
        idx end_neg = start_neg + 15;
        link_out_JETS[i+8].range(end_neg, start_neg) = NegRCTTwr2PF[i].towers[j].data;
        link_out_JETS[i+8].range(end_pos, start_pos) = PosRCTTwr2PF[i].towers[j].data;
		start_pos = start_pos + 16;
		start_neg = start_neg + 16;
	}
}

for(idx i=0; i<N_FIBERS_RCT; i++){
	#pragma HLS unroll
	link_out_JETS[i+12] = 0;
	idx start_neg = 0;
	idx start_pos = 272;
	for(idx j=0; j<N_RCTTOWERS_FIBER; j++){
		#pragma HLS unroll
        idx end_pos = start_pos + 15;
        idx end_neg = start_neg + 15;
        link_out_JETS[i+12].range(end_neg, start_neg) = NegRCTTwr3PF[i].towers[j].data;
        link_out_JETS[i+12].range(end_pos, start_pos) = PosRCTTwr3PF[i].towers[j].data;
		start_pos = start_pos + 16;
		start_neg = start_neg + 16;
	}
}

/*----------------------------PACKING THE PF TOWER in 2D Towers------------------------------*/

TowerPrimitives GCTTowersRegion[N_GCT_ETA][N_GCT_PHI];
#pragma HLS ARRAY_PARTITION variable=GCTTowersRegion complete dim=0

GCTPFPrimitives TotalPfclusterPOS[24];
GCTPFPrimitives TotalPfclusterNEG[24];
#pragma HLS ARRAY_PARTITION variable=TotalPfclusterPOS complete dim=0
#pragma HLS ARRAY_PARTITION variable=TotalPfclusterNEG complete dim=0

makePFTowerRegion(PosRCTTwr0PF, PosRCTTwr1PF, PosRCTTwr2PF, PosRCTTwr3PF, PosRCTTwr4PF,
                  NegRCTTwr0PF, NegRCTTwr1PF, NegRCTTwr2PF, NegRCTTwr3PF, NegRCTTwr4PF,
                  GCTTowersRegion);

makePfclusters(GCTTowersRegion, TotalPfclusterPOS, TotalPfclusterNEG);

/*--------------------PF LINKS PACKING----------------------*/

ap_uint<576> link_out_PF_reg[N_OUTPUT_LINKS];
#pragma HLS ARRAY_PARTITION variable=link_out_PF_reg complete dim=0

for(idx i=0; i<N_OUTPUT_LINKS/2; i++){
        #pragma HLS unroll
        link_out_PF_reg[i] = 0;
        idx start_pf = 0;
        for(idx j=0; j<N_PFCLUSTERS_FIBER; j++){
                #pragma HLS unroll
                idx end_pf = start_pf + 63;
                idx adrs = i*N_PFCLUSTERS_FIBER + j;
                ap_uint<64> data = 0;
                data.range(23, 0) = TotalPfclusterPOS[adrs].data;
                link_out_PF_reg[i].range(end_pf, start_pf) = data;
                start_pf = start_pf + 64;
        }
}

for(idx i=0; i<N_OUTPUT_LINKS/2; i++){
        #pragma HLS unroll
        link_out_PF_reg[i+12] = 0;
        idx start_pf = 0;
        for(idx j=0; j<N_PFCLUSTERS_FIBER; j++){
                #pragma HLS unroll
                idx end_pf = start_pf + 63;
                idx adrs = i*N_PFCLUSTERS_FIBER + j;
                ap_uint<64> data = 0;
                data.range(23, 0) = TotalPfclusterNEG[adrs].data;
                link_out_PF_reg[i+12].range(end_pf, start_pf) = data;
                start_pf = start_pf + 64;
        }
}

prsOutLinksCORR(link_out_PF_reg, link_out_PF);

}
